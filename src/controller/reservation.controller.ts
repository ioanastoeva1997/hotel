import { Body, Controller, Delete, Get, Param, Patch, Post, Query } from "@nestjs/common";
import { DSAEncoding } from "crypto";
import { Reservation } from "src/database/entities/reservation.entity";
import { ReservationDTO } from "src/dto/reservation.dto";
import { ReservationUpdateDTO } from "src/dto/reservationUpdate.dto";
import { threadId } from "worker_threads";
import { ReservationService } from "../service/reservation.service";

@Controller('/reservation')
export class ReservationController {
    public constructor(
        private reservationService: ReservationService
    ) {

    }

    @Post('') //asign - da go zapishesh
    public async createReservationToClient(// @Param() idCustomer: string,
        @Body() Reservation_Service: ReservationDTO,
    ) {
        console.log("a= ");console.log(Reservation_Service);
          return await this.reservationService.createReservationToClient(Reservation_Service);
    }

    @Delete(':id')
    async deleteReservation(
        @Param('id') id: string,
    ) {
        return await this.reservationService.DeleteReservation(id)
    }
    @Patch(':id')
    async Update_Reservation(
        @Param('id') id: string,
        @Body() updateReservation: ReservationUpdateDTO
    ) {
        return await this.reservationService.UpdateReservation(id, updateReservation);
    }

    @Get('/sd')
    async getDateAtReservation(
        @Query('sd') startDate: Date) {
        //console.log(Date().toString().slice(3,15));// Jul 30 2021
        // console.log(startDate);
        return await this.reservationService.getStartDateReservationService(startDate);
}
    @Get('/period')
    async getPeriodReservation(
        @Query('period') period: number) {
           // console.log(period);
        return await this.reservationService.getPeriodReservationService(period);
    }
    
    @Get('/otherBill')
    async getGeneralBill(
        @Query('idCustomer') idCustomer: string,
        @Query('ldate') lastDate:Date,
        ) {
        return await this.reservationService.getCustomerForGeneralBillService(idCustomer, lastDate);//, startDate, lastDate
    }
    @Get('/ltr')
    async getLastTwoReservation(
        @Query('id') idCustomer: string) {
        return await this.reservationService.getLastTwoReservationService(idCustomer);
    }
    @Get('frr')// must be (type: string, date:Date, period: number)
    async getFreeRoomReservation(
       // @Query('type') type: string, 
       @Query('type') types: string, 
       @Query('sd') startDate: Date, 
       @Query('ld') lastDate: Date) {
           // console.log(type);
        return await this.reservationService.getFreeRoomReservationService(types, startDate, lastDate);
    }
    @Get('')
    async getNumberReservationToEmployeeForPeriod(
        @Query('idE') idEmployee: string,
        @Query('sd') startDate: Date, 
        @Query('ld') endDate: Date
        ) {
        return await this.reservationService.getNumberReservationToEmployeeForPeriodService(idEmployee, startDate, endDate);
    }
}