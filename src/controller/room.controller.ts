import { Body, Delete, Get, Param, Patch, Query } from "@nestjs/common";
import { Controller, Post } from "@nestjs/common";
//import { Client } from "src/database/entities/client.entity";
import { Room } from "src/database/entities/room.entity";
import { RoomDTO } from "src/dto/room.dto";
//import { ClientService } from "../service/client.service";
import { RoomService } from "../service/room.service";

@Controller('/room')

export class RoomController {
    public constructor(
        private roomService: RoomService
    ) {

    }

    @Post('')
    public async createRoom(
        @Body() newRoom: RoomDTO,
    ) {
        return await this.roomService.createRoom(newRoom)
    }

    @Patch(':id')//pat parametyr                 try http://localhost:3000/room/9/room
    async updatePriceRoom(
        @Param('id') id: string,  //stoinost ot DTO
        @Body() body: { price: number },
    ) {
        return await this.roomService.UpdateRoom(id, body.price)
    }

    @Delete(':id')
    async DeleteRoom(
        @Param('id') id: string
    ) {
        return await this.roomService.DeleteRoom(id);
    }

    @Get('')
    async getTypeRoomC(

        @Query('type') type: string
    ) {
        console.log(type);
        return await this.roomService.getTypeRoomService(type);
    }


}
