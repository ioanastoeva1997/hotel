import { Body, Controller, Delete, Get, Param, Patch, Post, Query} from "@nestjs/common";
import { userDTO } from "src/dto/User.dto";
import { UserService } from "src/service/user.service";

@Controller("/user")
export class UserController {
  public constructor(private userService: UserService) {}
  @Post("")
  public async createUser(
    @Body() newUser: userDTO
    ) {
    return await this.userService.entreDatas(newUser);
  }

  @Patch(':id')
  public async update_User(
    @Param('id') id: string,
    @Body() updateUser: { phone: string, password: string,}
  ){
    return await this.userService.UpdateUser(id, updateUser);
  }

  @Delete(':id')
  public async deleteUser(
    @Param('id') id:string
    ){
    return await this.userService.DeleteUser(id);
  }

  @Get('')
  public async getUserByPhone(
    @Query('phone') phone: string,
  ){
    return await this.userService.GetUserByPhone(phone);
  }

  @Get('/name')
  public async getUserByName(
    @Query('name') name: string,
  ){
    return await this.userService.GetUserByName(name);
  }

  @Get('/role')
  public async getRole(
    @Query('role') role: string,
  ){
    return await this.userService.GetRole(role);
  }
  
}