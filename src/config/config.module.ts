import { Module } from '@nestjs/common';
import { ConfigService } from './config.servise';


@Module({
    providers: [ConfigService],
    exports: [ConfigService],
  })
  export class ConfigModule { }