export class ReservationUpdateDTO {
    public reservedAt: string;
    public reservedAtEnd: string;
    public period: number;
}