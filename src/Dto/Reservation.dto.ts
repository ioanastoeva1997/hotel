export class ReservationDTO {
    public reservedAt: Date;
    public reservedAtEnd: Date;
    public period: number;
    public idUserCustomer: string;
    
    public idUserEmployee: string;
    public idRoom: string; 
    public paidOrNot: false;
}