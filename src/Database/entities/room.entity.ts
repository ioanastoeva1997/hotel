
import { PrimaryGeneratedColumn, Column, Entity, OneToMany } from 'typeorm';
import { Reservation } from './reservation.entity';

@Entity('room')
export class Room {
    @PrimaryGeneratedColumn('uuid')
    public RoomId: string;

    @Column('int')
    public RoomNumber: number;
   
    @Column()
    public price: number;
  
    @Column()//'nvarchar'
    public type: string;

@OneToMany(type => Reservation, reservation => reservation.room)
public reservations: Promise<Reservation[]>;
}
