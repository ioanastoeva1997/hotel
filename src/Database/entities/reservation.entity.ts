import { PrimaryGeneratedColumn, Column, Entity, ManyToOne, JoinTable} from 'typeorm';
import { Room } from './room.entity';
import { User } from './user.entity';

@Entity('reservation')
export class Reservation {
  @PrimaryGeneratedColumn('uuid')
  public ReservationId: string;

  @Column('date')
  public reservedAt: Date;

  @Column('date')
  public reservedAtEnd: Date;

  @Column()
  public period: number;

  @Column()
  public paidOrNot: boolean;

  @ManyToOne((type) => User, (user) => user.reservations_client)
  @JoinTable() ///1 klient vzema/ima mnogo reservacii
  public client: Promise<User>;


  @ManyToOne((type) => User, (user) => user.reservations_employee)
  @JoinTable() ///1 klient vzema/ima mnogo reservacii
  public employee: Promise<User>;

  @ManyToOne((type) => Room, (room) => room.reservations)
  @JoinTable()
  public room: Promise<Room>;

}
