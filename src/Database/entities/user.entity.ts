import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from "typeorm";
import { Reservation } from "./reservation.entity";

@Entity('user')
export class User{
    @PrimaryGeneratedColumn('uuid')
    public userId: string;

    @Column('text')
    public name: string;

    @Column()
    public phone: string;
    
    @Column()
    public email: string;

    @Column()
    public password: string;
    
    @Column()
    public userName: string;

    @Column()
    public role: string;
 
    @OneToMany(type => Reservation, reservation => reservation.client)// client e pyrvata chast koqto se poqwqwa v kolonata na vryzkata i vtorata e ot koleto id v User
    public reservations_client: Promise<Reservation[]>;

    @OneToMany(type => Reservation, reservation => reservation.employee)
    public reservations_employee: Promise<Reservation[]>;
}