import { Repository } from 'typeorm';
import { Injectable } from "@nestjs/common";
import { InjectRepository } from '@nestjs/typeorm';
import { User } from 'src/database/entities/user.entity';
import { userDTO } from 'src/dto/User.dto';
import { UserUpdateDTO } from 'src/Dto/UserUpdate.dto';

@Injectable()   
// enum role {
//    client,
//    employee,
// }
export class UserService {
   constructor(
        @InjectRepository(User) 
        private userRepository: Repository<User>,
   ) {}
      
   entreDatas(entre:userDTO):Promise<User>{
      return this.userRepository.save(entre);
   }// async login(newlogin:loginDTO):Promise<any> {}

   async UpdateUser(id: string, user: UserUpdateDTO): Promise<User> {
      const foundUser = await this.userRepository.findOneOrFail({
       where: {
         userId:id,
       },
     });
     foundUser.phone = user.phone;
     foundUser.password = user.password;
     console.log(foundUser.password);
     return await this.userRepository.save(foundUser);
   }

   async DeleteUser(id:string):Promise<void>{
      const foundUser = await this.userRepository.findOneOrFail({
         where: {
           userId:id,
         },
       });
        await this.userRepository.delete(id);
   }

   async GetUserByPhone(phone:string):Promise<User>{
      const foundUserPhone = await this.userRepository.findOneOrFail({
         where: {
           phone: phone,
         },
       });
      return foundUserPhone;
   }

   async GetUserByName(name:string):Promise<User>{
      const foundUserName = await this.userRepository.findOneOrFail({
         where: {
           name: name,
         },
       });
      return foundUserName;
   }

   async GetRole(role:string):Promise<User[]>{
      const foundRole = await this.userRepository.find({
         where: {
           role: role,
         },
       });
      return foundRole;
   }
}