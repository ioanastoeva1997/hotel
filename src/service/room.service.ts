import { Injectable} from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { Room } from "src/database/entities/room.entity";
import { RoomDTO } from "src/dto/room.dto";
import { Repository } from "typeorm";

@Injectable()
export class RoomService {
    constructor(
        @InjectRepository(Room)
        private roomRepository: Repository<Room>,//roomRepository-object ot class Room
    ) {

    }

    createRoom(newRoom: RoomDTO): Promise<Room> {
        return this.roomRepository.save(newRoom);
    }

    async UpdateRoom(id: string, priceRoom: number): Promise<any> {
        const foundRoom: Room = await this.roomRepository.findOneOrFail({
            where: {
                RoomId: id,
            },
        });
        foundRoom.price = priceRoom;//DB = DTO
        return await this.roomRepository.update(id,{price: priceRoom})
    }

    async DeleteRoom(Id: string): Promise<void> {console.log("deleteS2= "+Id);
        const foundRoom: Room = await this.roomRepository.findOneOrFail({
            where: {
                RoomId: Id,
            }
        });
        console.log("delete= "+Id);
        await this.roomRepository.delete(Id);
    }

    async getTypeRoomService(type: string): Promise <Room[]>{
        const foundTypeRoom: Room[] = await this.roomRepository.find({
            where:{
                type:type,
            }
        });
        return foundTypeRoom;
    }

   
}