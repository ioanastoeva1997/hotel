import { Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { Reservation } from "src/database/entities/reservation.entity";
import { Room } from "src/database/entities/room.entity";
import { ReservationDTO } from "src/dto/reservation.dto";
import { ReservationUpdateDTO } from "src/dto/reservationUpdate.dto";
import { In, LessThanOrEqual, MoreThanOrEqual, Raw, Repository } from "typeorm";
import { User } from "src/database/entities/user.entity";
@Injectable()
export class ReservationService {
  constructor(
    @InjectRepository(Reservation)
    private reservationRepository: Repository<Reservation>,
    @InjectRepository(Room) private roomRepository: Repository<Room>,
    @InjectRepository(User) private userRepository: Repository<User>
  ) {}

  async createReservationToClient(
    newReservation: ReservationDTO
  ): Promise<any> {
    const foundClient = await this.userRepository.findOneOrFail({
      where: {
        userId: newReservation.idUserCustomer,
        role: "client",
      },
    });
    const foundEmployee = await this.userRepository.findOneOrFail({
      where: {
        userId: newReservation.idUserEmployee,
        role: "employee",
      },
    }); // ako e svobodna

    const foundRoom = await this.roomRepository.findOneOrFail({
      where: {
        //getFreeRoomReservationService
        RoomId: newReservation.idRoom,
      },
    }); //const validateToReservation = await this.reservationRepository.find({where: {reservedAtEnd: LessThan(newReservation.reservedAt),reservedAt: MoreThan(newReservation.reservedAtEnd),},});
    //////referenciite sa razlichni - stojnostite sa ednakvi no v razlichni kletki-> ne tyrsish po celiq obekt a samo po id
    const freeRooms = await this.getFreeRoomReservationService(
      foundRoom.type,
      newReservation.reservedAt,
      newReservation.reservedAtEnd
    );
    const idRoom = foundRoom.RoomId;
    const idFreeRooms = freeRooms.map((x) => {
      //const d= x.RoomId;
      const id = x.map((z) => {
        const idRoom = z.RoomId;
        return idRoom;
      });
      return id.toString();
    });
    console.log("freeRooms= ");
    console.log(freeRooms);
    console.log("idFreeRooms= ");
    console.log(idFreeRooms);
    console.log("idRoom= ");
    console.log(idRoom);
    console.log("idFreeRooms.includes(idRoom)= ");
    console.log(idFreeRooms.includes(idRoom));

    if (idFreeRooms.includes(idRoom)) {
      // var todayDate = new Date().toISOString().slice(0, 10);
      // if (newReservation.reservedAt.toString() < todayDate) {
      //   throw new Error('can not start date to less more from today date');
      // }
      //free room  oshte ot nachaloto
      const reservation = {
        reservedAt: newReservation.reservedAt,
        reservedAtEnd: newReservation.reservedAtEnd,
        period: newReservation.period,
        paidOrNot: false,
      }; //false=0; true=1

      const a = this.reservationRepository.create(reservation);
      a.client = Promise.resolve(foundClient);
      a.employee = Promise.resolve(foundEmployee);
      a.room = Promise.resolve(foundRoom);
      return await this.reservationRepository.save(a);
    } else {
      /////////////////////////////don't work////////////////////////////////////////////
      if (idFreeRooms !== []) {
        return [
          "this rooms which are from same type are  free for this period ",
          freeRooms,
        ];
      } else if (idFreeRooms === []) {
        throw new Error("the rooms by this type is not free for this period");
      }
    }
  }

  async DeleteReservation(id: string): Promise<void> {
    const foundReservation: Reservation =
      await this.reservationRepository.findOneOrFail({
        where: {
          ReservationId: id,
        },
      });
    await this.reservationRepository.delete(id);
  } //r1-old,r2-current()-nastoqsht,r3- future //r2_End >= nastoqshtata_data- za udyljavane na reservaciq v syshtata staq ili nova//r3_Start > current_date

  async UpdateReservation(
    id: string,
    updateReservat: ReservationUpdateDTO
  ): Promise<any> {
    //!!!!!!!!!!!!!!!!CHECK!!!!!!!!!!!!!!!!!!!!!!!
    const foundReservation: Reservation =
      await this.reservationRepository.findOneOrFail({
        where: {
          // vzemame vsichki ID-ta i tyrsim syvpada li
          ReservationId: id, // reservedAtEnd: LessThan(updateReservat.reservedAt),// reservedAt: MoreThan(updateReservat.reservedAtEnd),
        },
      });
    //  foundReservation.period = updateReservat.period;
    //foundReservation.reservedAt = updateReservat.reservedAt;////////////////////
    //return await this.reservationRepository.save(updateReservt);ako syshtestwuwa id-to save samo go update-va
    return await this.reservationRepository.update(id, {
      period: updateReservat.period,
      reservedAt: updateReservat.reservedAt,
      reservedAtEnd: updateReservat.reservedAtEnd,
    });
  }

  async getStartDateReservationService(
    startDate: Date
  ): Promise<Reservation[]> {
    // console.log(startDate);
    const foundStartDate: Reservation[] = await this.reservationRepository.find(
      {
        where: {
          reservedAt: startDate,
        },
      }
    );
    return foundStartDate;
  }

  async getPeriodReservationService(periodReservation: number): Promise<any> {
    const foundPeriodReservation = await this.reservationRepository.find({
      where: {
        period: periodReservation,
      },
    });
    return foundPeriodReservation;

    // const user = await (await foundPeriodReservation.client).userId;const employee = await (await foundPeriodReservation.employee).userId;return [foundPeriodReservation.ReservationId,foundPeriodReservation.period,foundPeriodReservation.reservedAt,//.toLocaleDateString(),foundPeriodReservation.reservedAtEnd,//.toLocaleDateString(),user,employee,room,];
  } /*  const a = this.reservationRepository.create(reservation);a.client = Promise.resolve(foundClient);a.employee = Promise.resolve(foundEmployee);              a.room = Promise.resolve(foundRoom);return await this.reservationRepository.save(a); */

  async getCustomerForGeneralBillService(
    idClient: string,
    lastDate: Date
  ): Promise<any> {
    const foundIdClient = await this.userRepository.findOne({
      where: {
        userId: idClient,
      },
    });
    if (!foundIdClient) {
      //undefine
      throw new Error("does not have that a customer");
    }
    const foundpayed = await this.reservationRepository.find({
      where: {
        paidOrNot: false,
        client: foundIdClient.userId,
      },
    });
    const billToPay = foundpayed.filter((pay) => pay.paidOrNot == false);
    //  const allReservationToClient = await foundIdClient.reservations;
    const result = billToPay.filter((word) => word.reservedAtEnd >= lastDate);
    const PeriodReservationz = await Promise.all(
      result.map((reservation) => reservation.period)
    );
    const RoomReservation = await Promise.all(
      result.map((reservation) => reservation.room)
    );
    const RoomReservationPrice = await Promise.all(
      RoomReservation.map((reservation) => reservation.price)
    );
    const generalBill = await PeriodReservationz.map(
      (x, index) => RoomReservationPrice[index] * x
    ); //(x => x * Number(RoomReservationPrice));->this is error
    // const updateStatusPay= await this.reservationRepository.update({reservedAtEnd: lastDate, client: foundIdClient.clientId}, {paidOrNot: true});//???????
    //const _foundReservation= await this.reservationRepository.findOne({where: {ReservationId: billToPay[0].ReservationId,},});
    const k = result.map((x) => (x.paidOrNot = true));
    await this.reservationRepository.save(result);
    return generalBill;

    //return await this.reservationRepository.update(id, { period: updateReservat.period, reservedAt: updateReservat.reservedAt, reservedAtEnd: updateReservat.reservedAtEnd });
    //return await this.reservationRepository.update(lastDate, {paidOrNot: true});
    /*
    var a = [1,2,3,4,5];var b = [5,4,3,2,1];
    a.map(function(x, index){ //here x = a[index]
     return b[index] + x });
    =>[6,6,6,6,6]
    //if you want to add the elements of an array:
    a.reduce(function(x, y){return x + y});
    =>15*/
    // const lastReservationToClient: Reservation = allReservationToClient[allReservationToClient.length - 1];
    // const periodReservation = lastReservationToClient.period;
    // const roomToClient = lastReservationToClient.room; //foundIdRoom.price;
    // const priceRoom = (await roomToClient).price;
    // const generalBill = periodReservation * priceRoom;
    // return generalBill;
  }

  async getLastTwoReservationService(idUser_client: string): Promise<any> {
    const foundClient = await this.userRepository.findOneOrFail({
      where: {
        userId: idUser_client,
        role: "client",
      },
    });
    const allReservationToClient = await foundClient.reservations_client; //all reservation to customer
    const lastReservationToClient =
      allReservationToClient[allReservationToClient.length - 1]; //see which is true row 113 or 114!!!!!!!!!!!
    const secondToLastReservationToClient =
      allReservationToClient[allReservationToClient.length - 2]; //!!!!([allReservationToClient.length-1] or [length-2] or allReservationToClient.reverse())
    return [lastReservationToClient, secondToLastReservationToClient];
  }

  async getFreeRoomReservationService(roomType: string,startDate: Date,lastDate: Date): Promise<any> {
    if(lastDate<startDate) throw new Error("The last date can't to it is less from the start date.");
    const foundRoom: Room[] = await this.roomRepository.find({
      relations: ["reservation"],
      where:
         [
          {
             reservation:{
              reservedAt: LessThanOrEqual(startDate && lastDate),
             reservedAtEnd: MoreThanOrEqual(startDate && lastDate),  
             },
             type: roomType,
           },
          {
            reservation:{
              reservedAtEnd: LessThanOrEqual(lastDate),
            reservedAt: MoreThanOrEqual(startDate),
            },
            type: roomType,
          },
         ],// where: {type: roomType,},
    });
        console.log("rooms= ");console.log(foundRoom);


    const idRoomsByType = foundRoom.map((x) => x.RoomId);
    const foundReservation = await this.reservationRepository.find({
        relations: ["room"],
        where:
        [
          {
             reservedAt: LessThanOrEqual(startDate && lastDate),
             reservedAtEnd: MoreThanOrEqual(startDate && lastDate),
             room:{
               RoomId: In (idRoomsByType),
             }
           },
          {
            reservedAtEnd: LessThanOrEqual(lastDate),
            reservedAt: MoreThanOrEqual(startDate), 
            room:{
              RoomId: In (idRoomsByType),
            }
          },
        ]
    });
    const comparetoidroom = await Promise.all(foundReservation.map(async (o) => (await o.room).RoomId));
    const idRoomFree= idRoomsByType.filter((IdRoomIsFree) => !comparetoidroom.includes(IdRoomIsFree));
    const freeRooms=  foundRoom.filter((freeRooms)=>idRoomFree.includes(freeRooms.RoomId));
    
    if (freeRooms !== []) {
      return freeRooms;
    } else {
      throw new Error("Don't have free room from this type");
    }
  }

  async getNumberReservationToEmployeeForPeriodService(
    idUser_Employee: string,
    startDate: Date,
    endDate: Date
  ): Promise<any> {
    const array = [];
    const foundByPeriod: Reservation[] = await this.reservationRepository.find({
      relations:["employee"],
      where: {
        reservedAt: MoreThanOrEqual(startDate),
        reservedAtEnd: LessThanOrEqual(endDate),
       employee:{
        userId: idUser_Employee,
        role: "employee",
       }
      },
    }); 
    return foundByPeriod.length;
  }

}