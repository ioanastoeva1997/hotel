import { UserModule } from './modules/user.module';
import { RoomModule } from './Modules/room.module';
import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { DatabaseModule } from './database/database.module';
import { ReservationModule } from './modules/reservation.module';

@Module({
  imports: [DatabaseModule, ReservationModule, RoomModule, UserModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule { }
