import { Reservation } from 'src/database/entities/reservation.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Module } from '@nestjs/common';
import { UserController } from 'src/controller/user.controller';
import { UserService } from 'src/service/user.service';
import { User } from 'src/database/entities/user.entity';

@Module({
    imports: [TypeOrmModule.forFeature([User,Reservation])],
    controllers: [UserController],
    providers: [UserService],
    exports: [UserService]
})
export class UserModule{
    
}