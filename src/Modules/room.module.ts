import { Reservation } from 'src/database/entities/reservation.entity';
import { Room } from './../database/entities/room.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Controller } from '@nestjs/common';
import { Module } from '@nestjs/common';
import { RoomController } from 'src/controller/room.controller';
import { RoomService } from 'src/service/room.service';

@Module({
    imports:[TypeOrmModule.forFeature([Room,Reservation])],
    controllers:[RoomController],
    providers:[RoomService],
    exports:[RoomService]
})

export class RoomModule{
    
}
