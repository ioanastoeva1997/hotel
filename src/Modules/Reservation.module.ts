import { Module } from '@nestjs/common';
import { TypeOrmModule } from "@nestjs/typeorm";
import { Reservation } from "src/database/entities/reservation.entity";
import { Room } from "src/database/entities/room.entity";
import { User } from 'src/database/entities/user.entity';
import { ReservationController } from "../controller/reservation.controller";
import { ReservationService } from "../service/reservation.service";


@Module({          /////////forFeature- use about entities
    imports: [TypeOrmModule.forFeature([Reservation, Room, User])],
    controllers: [ReservationController],
    providers: [ReservationService],
    exports: [ReservationService]
})
export class ReservationModule {
    
}

/*ALL
WHAT TO DO?*/